'use strict';

if (confirm("Do you wanna play a little game?))")) {
  what(getSide(), getSide());
} else {alert("Ну, нет так нет...")}


function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function getNumber(b) {
  let a;
  do {
    a = prompt("Enter " + b +" coordinate of your spot", '3');
  } while (!isNumeric(a));
  return a;
}

function getSide() {
  let a;
  do {
    a = prompt("Enter parameter of your figure", '3');
  } while (!isNumeric(a));
  return a;}

function what (radius, squareSide) {

    let abscis = "x";
    let ordinates = "y";

    let x = getNumber(abscis);
    let y = getNumber(ordinates);

  if (x < squareSide / 2 && y < squareSide / 2 && Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) > radius) {
    alert("Yes, your spot lies between circle and square")
  } else {
    alert("No, your spot probably is inside of circle")
  }
}

