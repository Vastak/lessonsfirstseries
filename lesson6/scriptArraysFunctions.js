'use strict'

let goods = [{
  name: 'pen',
  price: 10,
  category: 'writing'
},
  {
  name: 'pencil',
  price: 5,
  category: 'writing'
  },
  {
    name: 'rubber',
    price: 20,
    category: 'non-writing'
  },
  {
    name: 'scisors',
    price: 15,
    category: 'non-writing'
  },
  {
    name: 'paper',
    price: 12,
    category: 'non-writing'
  },
  {
    name: 'marker',
    price: 18,
    category: 'writing'
  }];
//deletes goods by name
function deleteGoods (arr) {
arr = arr.filter(item => item.name !== 'pen')
return arr; }

function incrementalSort (array) {
 let inctrementalArray =  array.sort(function (a,b) {
if (a.price > b.price)
  return 1;
if (a.price < b.price)
  return -1;
return 0;
  })
  return inctrementalArray;
}

function decrementalSort (array) {
  let decrementalArray =  array.sort(function (a,b) {
    if (a.price > b.price)
      return -1;
    if (a.price < b.price)
      return 1;
    return 0;
  })
  return decrementalArray;
}
//Sorts all goods of category by price
function categoryIncrementalSort(array, categor) {
let arrayCategory = array.filter(item => item.category === categor);
arrayCategory = arrayCategory.sort((a,b) => {
if (a.price > b.price)
  return 1;
if (a.price < b.price)
  return -1;
return 0;
})
 return arrayCategory;
}
//Filters all good`s prices in determined range and summarize them
function priceFilterSum(array, a, b) {
let priceFilter = array.filter(item => item.price > a && item.price < b)
let summary = priceFilter.reduce(((sum, current) => sum + current.price), 0);
  return summary;
}
//Filters all goods by category and summarize their prices
function priceCategorySum(array, categor) {
  let priceFilter = array.filter(item => item.category == categor);
  let summary = priceFilter.reduce(((sum, current) => sum + current.price), 0);
  return summary;
}
